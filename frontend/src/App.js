import React, { useState } from 'react';
import './App.css';

function App() {
  const [result, setResult] = useState('');

  const fetchInfo = async () => {
    console.log(process.env.REACT_APP_API_URL);
    const url = `${process.env.REACT_APP_API_URL}/api/test`;

    const response = await fetch(url);
    const json = await response.json();
    setResult(json);
  };

  return (
    <div className="App">
      <button onClick={fetchInfo}>fetch info from API</button>
      <div>result:{result}</div>
    </div>
  );
}

export default App;
